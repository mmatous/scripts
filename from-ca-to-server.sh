# generate key
sudo openssl ecparam -out ca-key.pem -name secp384r1 -genkey
# generate certificate signing request
sudo openssl req -config dassem-ca.conf -new -key ca-key.pem -out ca-cert-req.pem -sha384 -extensions v3_ca
# sign request
sudo openssl x509 -in ca-cert-req.pem -out ca-cert.pem -req -signkey ca-key.pem -days 365 -extfile dassem-ca.conf  -extensions v3_ca
# verify output
sudo openssl x509 -in ca-cert.pem -text -noout

# generate key for server
sudo openssl ecparam -out glados-key.pem -name secp384r1 -genkey
# generate request
sudo openssl req -config dassem-ca.conf -new -key glados-key.pem -out glados-cert-req.pem -sha384 -extensions v3_req
# sign it with our CA
sudo openssl ca -in glados-cert-req.pem -out glados-cert.pem -config dassem-ca.conf -extensions v3_req -policy signing_policy

#config file used:
HOME            = .
RANDFILE        = /root/.rnd

####################################################################
[ ca ]
default_ca    = CA_default      # The default ca section

[ CA_default ]

default_days     = 1000         # how long to certify for
default_crl_days = 30           # how long before next CRL
default_md       = sha384       # use public key default MD
preserve         = no           # keep passed DN ordering

x509_extensions = v3_ca  # The extensions to add to the cert

email_in_dn     = no            # Don't concat the email in the DN
copy_extensions = copy          # Required to copy SANs from CSR to cert

####################################################################
[ req ]
default_bits       = 384
default_keyfile    = ca-key.pem
distinguished_name = ca_distinguished_name
x509_extensions    = v3_ca
req_extensions	   = v3_req
string_mask        = utf8only

####################################################################
[ ca_distinguished_name ]
countryName         = Country Name (2 letter code)
countryName_default = ME

stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = Malazan Empire

localityName                = Locality Name (eg, city)
localityName_default        = Malaz City

organizationName            = Organization Name (eg, company)
organizationName_default    = Malazan military forces

organizationalUnitName         = Organizational Unit (eg, division)
organizationalUnitName_default = "Dassem's First Sword"

commonName         = Common Name (e.g. server FQDN or YOUR name)
commonName_default = Dassem Ultor

emailAddress         = Email Address
emailAddress_default = dassem@dessembrae.com

####################################################################
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment

[ v3_ca ]
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid:always, issuer
basicConstraints       = critical, CA:TRUE, pathlen:0
keyUsage               = keyCertSign, cRLSign

