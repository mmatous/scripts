#!/usr/bin/env python

import os
import subprocess
import sys

for root, dirs, files in os.walk(sys.argv[1]):
    for name in files:
        input_name, input_ext = os.path.splitext(name)
        l_input_ext = input_ext.lower()
        if l_input_ext != '.m4a':
            continue
        full_name_i = os.path.join(root, name)
        full_name_o = os.path.join(root, input_name + '.flac')
        process = subprocess.run(['ffmpeg', '-y', '-i', full_name_i, '-compression_level', '12', full_name_o],
                                                    check=True,
                                                    capture_output=True)
        print('deleting ', full_name_i)
        print('out ', full_name_o)
        os.remove(full_name_i)
