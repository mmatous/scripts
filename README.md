# Various useful-ish scripts
Scripts I wrote at some point or another. Inconsistent quality, no guarantees.

It's likely some never worked or that I got bored writing halfway.

## availability-monitor.py
Low-budget homemade handmade monitoring for homeserver.

Status: active use

Dependencies: python, dnspython, Matrix account

Usage: Run as a daemon, `/usr/local/bin/availability-monitor.py <interval-sec>`

## dnf-search-install.py
Wrapper, marks already installed packages for `dnf search`. Slow.

Status: active use

Dependencies: dnf, python

Setup:
```bash
sudo cp ./dnf-search-installed.py /usr/local/bin/.
alias -s dnf '/usr/local/bin/dnf-search-installed.py'
```
Usage: `dnf search <package>`


## ensure-port-fwd.py
Ensure given ports are present in TC7200 port forwarding table.

For all those who get shitty routers shoved down their throats
by their greedy and incompetent ISPs who like to randomly
reset their settings for fun and profit using a backdoor.

Status: active use (🖕 UPC/Vodafone)

Dependencies: selenium, geckodriver, e-waste

Usage: Run as a systemd service


## gasquery.py
Query Alchemy API for current ETH L1 basefee.
Intended for consumption by i3status-rs' custom block.

Status: active use

Dependencies: Alchemy API key

Usage: `gasquery.py <alchemy-api-key> <notification-threshold>`


## gentoo-chroot.sh
Automate chrooting from live USB to fix installed system.

Status: active use :(

Dependencies: Nothing unusual

Usage: `chroot.sh`


## gtokei.sh
Wrapper, count lines of code for git repos with [tokei](https://github.com/XAMPPRocky/tokei).

Status: active use

Dependencies: tokei, git

Usage: `gtokei.sh https://github.com/some/repo`


## kernel-update.py
Automate chores when configuring, compiling and updating kernel.

Status: active use

Dependencies: dracut, Gentoo (not really but eselect, specific names and paths...)

Usage: `kernel-update.py update <old-version> <new-version>`


---

## flac-convert.py
Convert all .m4a into max-compressed .flac

Status: ancient one-off, likely low quality

Dependencies: ffmpeg

Usage: `flac-convert.py /path/to/music`


## from-ca-to-server.sh
Create own CA and use it to sign a certificate

Status: ancient one-off, unknown purpose

Dependencies: openssl

Usage: ???


## jxl-convert.py
Recursively convert jpgs to jxls with additional checks.

Status: one-off

Dependencies: libjxl

Usage: `./jxl-convert.py /path/to/pics`


## invoke-magic.bat
Add watermark to photo and create thumbnail.
Contains detailed parameter explanation. Created for friend's blog.

Status: ancient but probably still working

Dependencies: imagemagick

Usage: Run next to `logo.png` and `workdir` directory with photos


## sync-apparmor.py
Scan source directory of profiles and binaries in the system.

Copy profiles that have a corresponding binary from src to dst.
Clean profiles that don't have a corresponding binary from dst.

Useful for initial profile dir setup and mopping up old profiles.

Doesn't work for subprofiles

Status: one-off

Dependencies: apparmor

Usage: `sync-apparmor.py <src> <dst>`


## try-luks-from-kdbx.py
Retrieve known password for a LUKS volume from a KeePass DB.

For those moments when you forget the name of or
mislabel your DB entry.

Status: one-off

Dependencies: pykeepass

Usage: `try-luks-from-kdbx.py /path/to/kdbx /path/to/block-device`