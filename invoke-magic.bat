@echo off

IF NOT EXIST ./\conv mkdir conv
FOR %%f in (./workdir/*.jpg) DO (
	magick convert "./workdir/%%f" ^
	-resize 1500 -quality 85  ^
	logo.png -gravity SouthEast -geometry +0+50 ^
	-composite "./conv/%%f"
)



IF NOT EXIST ./\thm mkdir thm
magick mogrify -path ./thm -thumbnail 250 -quality 85 ./workdir/*

:: Requires ImageMagick installed, modified PATH (gets done during install)
:: resize - [NUM]: width, height get computed; [xNUM]: height, width gets computed;
::		[NUM1xNUM] is max(NUM1,NUM2) that's aspect preserving
:: quality - compression, not consistent between various SW
:: gravity - where to put news element
:: geometry - offset from borders, X,Y
:: composite - compose in order of "background, foreground"
:: path - output, original gets replaced if omitted
:: thumbnail - optimalized resize + compression + strip
:: "*" means "gobble whatever you get your hands on"
:: strip - discard metadata
::