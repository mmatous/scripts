#!/usr/bin/env python

import json
import requests
import subprocess
import sys

API_KEY = sys.argv[1]
BASEFEE_THRESHOLD = int(sys.argv[2])

def to_gwei(wei: int) -> int:
	return int(wei/(10**9))

response = requests.post(
	url=f'https://eth-mainnet.alchemyapi.io/v2/{API_KEY}',
	data='{"jsonrpc":"2.0","method":"eth_gasPrice","params":[],"id":0}'
	)
if not response.ok:
	subprocess.run(['notify-send', 'Gasquery', f'Query returned {response.status_code}: {response.reason}'])
	print('{"text": "⚠️NaN"}')
	sys.exit()

basefee = response.json()['result']
basefee = to_gwei(int(basefee, base=16))
if basefee <= BASEFEE_THRESHOLD:
	subprocess.run(['notify-send', 'Gasquery', f'Current basefee is {basefee}'])

state = 'Idle'
if basefee < BASEFEE_THRESHOLD:
	state = 'Good'
elif basefee > 150:
	state = 'Warning'
elif basefee > 200:
	state = 'Critical'
text = '{' + f'"state": "{state}", "text": "⛽ {basefee}"' + '}'
print(text)
