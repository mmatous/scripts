#!/usr/bin/env bash

MOUNTPOINT=/mnt/gentoo

mkdir -p "${MOUNTPOINT}" &&
mount --types proc /proc "${MOUNTPOINT}"/proc &&
mount --rbind /sys "${MOUNTPOINT}"/sys &&
mount --make-rslave "${MOUNTPOINT}"/sys &&
mount --rbind /dev "${MOUNTPOINT}"/dev &&
mount --make-rslave "${MOUNTPOINT}"/dev &&
mount --bind /run "${MOUNTPOINT}"/run &&
mount --make-slave "${MOUNTPOINT}"/run &&
mount LABEL=BOOT "${MOUNTPOINT}"/boot &&
mount --rbind /tmp "${MOUNTPOINT}"/tmp &&
chroot "${MOUNTPOINT}" /bin/bash

# gentoo install media not running systemd
# systemd-nspawn -D /mnt/mychroot --bind=/tmp --resolv-conf=/etc/resolv.conf
