#!/usr/bin/env bash

tmpdir=$(mktemp --directory)
git clone --depth 1 "$1" "$tmpdir" && tokei "$tmpdir"
rm -rf "$tmpdir"
