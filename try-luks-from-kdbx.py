#!/usr/bin/env python3

import getpass
import subprocess
import sys

from pykeepass import PyKeePass

if len(sys.argv) < 3:
	print(f'Usage: {sys.argv[0]} /path/to/kdbx /path/to/block-device\n')

pwd = getpass.getpass('Enter database password:\n')
kp = PyKeePass(sys.argv[1], password=pwd)
cmd = ['cryptsetup', 'open', '--test-passphrase', sys.argv[2]]
for entry in kp.entries:
	res = subprocess.run(
		cmd,
		stdout=subprocess.DEVNULL,
		stderr=subprocess.DEVNULL,
		input=entry.password.encode()  # input accepts bytes, not str
		)
	if res.returncode == 0:
		print(f'Password found: {entry.title}\npw: {entry.password}')
		break
	else:
		print(f'Entry "{entry.title}" not a match')
